import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashMap;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class ChatMessagesPanel extends JPanel implements KeyListener {

	private GradientPanel panelTop = new GradientPanel(Const.BORDER_WIDTH, Const.COLOR_GREEN_LEFT,
			Const.COLOR_GREEN_MIDDLE, Const.COLOR_GREEN_RIGHT);
	private JPanel textFieldHolder = new JPanel();
	private GradientPanel panelBottom = new GradientPanel(Const.BORDER_WIDTH, Const.COLOR_GREEN_LEFT,
			Const.COLOR_GREEN_MIDDLE, Const.COLOR_GREEN_RIGHT);
	private GradientPanel panelSmallGap = new GradientPanel(Const.BORDER_WIDTH, Const.COLOR_WHITE_LEFT,
			Const.COLOR_WHITE_MIDDLE, Const.COLOR_WHITE_RIGHT);
	private GradientPanel panelAnotherSmallGap = new GradientPanel(Const.BORDER_WIDTH, Const.COLOR_WHITE_LEFT,
			Const.COLOR_WHITE_MIDDLE, Const.COLOR_WHITE_RIGHT);
	private GradientPanel panelTextFieldBorder = new GradientPanel(Const.BORDER_WIDTH, Const.COLOR_WHITE_LEFT,
			Const.COLOR_WHITE_MIDDLE, Const.COLOR_WHITE_RIGHT);

	private JLabel labelChatPartner = new JLabel("");
	private ResponsiveJTextField textField = new ResponsiveJTextField(Const.SEND_MESSAGE);

	private HashMap<String, Integer> scrollBarValues = new HashMap<String, Integer>();

	private ChatMessageScrollPanel messagePanel;

	private ChatGui chatGui;

	public ChatMessagesPanel(ChatGui chatGui) {
		super();
		this.chatGui = chatGui;
		messagePanel = new ChatMessageScrollPanel(chatGui);
		createGui();
	}

	public void createGui() {
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.add(panelTop);
		this.add(messagePanel);
		this.add(panelSmallGap);
		this.add(panelTextFieldBorder);
		this.add(panelAnotherSmallGap);
		this.add(panelBottom);

		labelChatPartner.setForeground(Color.WHITE);
		labelChatPartner.setFont(new Font("Courier New", Font.BOLD, 14));
		ChatGui.changeFontSize(labelChatPartner, chatGui.fontSize + 2);

		panelTop.setPreferredSize(new Dimension(ChatPartnersPanel.CHAT_WIDTH, 35));
		panelTop.setMaximumSize(new Dimension(9999, 35));
		panelTop.setMinimumSize(new Dimension(ChatPartnersPanel.CHAT_WIDTH, 35));
		panelTop.setLayout(new FlowLayout(FlowLayout.LEFT, 15, 8));
		panelTop.add(labelChatPartner);

		panelSmallGap.setPreferredSize(new Dimension(ChatPartnersPanel.CHAT_WIDTH, 5));
		panelSmallGap.setMaximumSize(new Dimension(9999, 5));
		panelSmallGap.setMinimumSize(new Dimension(ChatPartnersPanel.CHAT_WIDTH, 5));

		panelTextFieldBorder.setOpaque(false);
		panelTextFieldBorder.setBorder(new EmptyBorder(5, 10, 5, 15));
		panelTextFieldBorder.setMinimumSize(new Dimension(ChatPartnersPanel.CHAT_WIDTH - 25, 40));
		panelTextFieldBorder.setPreferredSize(new Dimension(ChatPartnersPanel.CHAT_WIDTH - 25, 40));
		panelTextFieldBorder.setMaximumSize(new Dimension(9999, 40));
		panelTextFieldBorder.setLayout(new BorderLayout());
		panelTextFieldBorder.add(textFieldHolder, BorderLayout.CENTER);
		panelTextFieldBorder.setVisible(false);

		textFieldHolder.setLayout(new BorderLayout());
		textFieldHolder.add(textField, BorderLayout.CENTER);
		textFieldHolder.setBackground(Const.COLOR_SELECTED_MIDDLE);

		textField.setOpaque(false);
		textField.setBorder(new EmptyBorder(5, 10, 5, 15));
		textField.setText("Text eingeben...");
		textField.addKeyListener(this);
		ChatGui.changeFontSize(textField, chatGui.fontSize);

		panelAnotherSmallGap.setPreferredSize(new Dimension(ChatPartnersPanel.CHAT_WIDTH, 5));
		panelAnotherSmallGap.setMaximumSize(new Dimension(9999, 5));
		panelAnotherSmallGap.setMinimumSize(new Dimension(ChatPartnersPanel.CHAT_WIDTH, 5));

		panelBottom.setPreferredSize(new Dimension(ChatPartnersPanel.CHAT_WIDTH, Const.BORDER_WIDTH));
		panelBottom.setMaximumSize(new Dimension(9999, Const.BORDER_WIDTH));
		panelBottom.setMinimumSize(new Dimension(ChatPartnersPanel.CHAT_WIDTH, Const.BORDER_WIDTH));

	}

	public void addNewChatMessage(Message message) {
		messagePanel.addChatMessage(message, labelChatPartner.getText());
	}

	public void changeChatPartner(String chatPartner) {
		if (Const.CHAT_PARTNER_SONSTIGE.equals(chatPartner) || Const.CHAT_PARTNER_SERVER.equals(chatPartner)
				|| "".equals(chatPartner)) {
			panelTextFieldBorder.setVisible(false);
		} else {
			panelTextFieldBorder.setVisible(true);
			textField.requestFocus();
		}

		messagePanel.showChatMessagesFrom(chatPartner, labelChatPartner.getText());
		labelChatPartner.setText(chatPartner);
	}

	public void setFontSizes(int size) {
		messagePanel.setFontSize(size);
		ChatGui.changeFontSize(textField, size);
		ChatGui.changeFontSize(labelChatPartner, size + 2);
	}

	public String getChatPartner() {
		return labelChatPartner.getText();
	}

	@Override
	public void keyTyped(KeyEvent e) {
		chatGui.onKeyTyped(e);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

}
