import javax.swing.JTextField;

public class ResponsiveJTextField extends JTextField {

	private String actionCommand;

	public ResponsiveJTextField(String actionCommand) {
		this.actionCommand = actionCommand;
	}

	public String getActionCommand() {
		return this.actionCommand;
	}
}
