import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Base64;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;

public class ChatGui extends JDialog {

	private EasyChat easyChat;

	private JPanel rootPanel = new JPanel();

	private NewChatDialog newChatDialog;
	private ChangeFontDialog changeFontDialog;
	private ChatMessagesPanel messagesChatPanel;
	private ChatPartnersPanel chatPartnersPanel;

	public int fontSize = 14;

	public ChatGui(EasyChat easyChat) {
		super();

		this.easyChat = easyChat;

		newChatDialog = new NewChatDialog(this);
		changeFontDialog = new ChangeFontDialog(this);
		messagesChatPanel = new ChatMessagesPanel(this);
		chatPartnersPanel = new ChatPartnersPanel(this);

		createGui();

		try {
			BufferedImage img = ImageIO
					.read(new ByteArrayInputStream(Base64.getDecoder().decode(Const.LOGO_BASE_64.getBytes())));
			this.setIconImage(img);
		} catch (IOException e) {
			e.printStackTrace();
		}

		this.setTitle("EasyChat [_Skewy]");
		this.setLocationRelativeTo(null);
		this.setAlwaysOnTop(true);
		this.setSize(new Dimension(470, 500));
		this.setMinimumSize(new Dimension(375, 195));
	}

	private void createGui() {
		this.getContentPane().add(rootPanel);

		rootPanel.setLayout(new BorderLayout());
		rootPanel.add(chatPartnersPanel, BorderLayout.WEST);
		rootPanel.add(messagesChatPanel, BorderLayout.CENTER);
	}

	public void addNewMessage(Message message) {
		chatPartnersPanel.addOrUpdateChatPartner(message.getChatPartner());
		messagesChatPanel.addNewChatMessage(message);
	}

	public void onMouseClicked(MouseEvent e) {
		if (e.getSource() instanceof ChatPartnerPanel) {
			ChatPartnerPanel sourcePanel = (ChatPartnerPanel) e.getSource();
			chatPartnersPanel.changeChatPartner(sourcePanel.getChatPartner());
			messagesChatPanel.changeChatPartner(sourcePanel.getChatPartner());
		} else if (e.getSource() instanceof ResponsiveJLabel) {
			ResponsiveJLabel optionLabel = (ResponsiveJLabel) e.getSource();
			if (Const.MENU_ADD_CHAT.equals(optionLabel.getMenuType())) {
				newChatDialog.setVisible(true);
				newChatDialog.focusTextField();
				newChatDialog.resetTextField();
			} else if (Const.MENU_CHANGE_FONT.equals(optionLabel.getMenuType())) {
				changeFontDialog.setVisible(true);
			}
		}
	}

	public void onMouseEntered(MouseEvent e) {
		if (e.getSource() instanceof ChatPartnerPanel) {
			ChatPartnerPanel panel = (ChatPartnerPanel) e.getSource();
			panel.onMouseEntered();
		} else if (e.getSource() instanceof JButton) {
			JButton button = (JButton) e.getSource();
			for (ChatPartnerPanel panel : chatPartnersPanel.getChatPartnerPanels()) {
				if (panel.getChatPartner().equals(button.getActionCommand())) {
					panel.onCloseButtonEntered();
				}
			}
		}
	}

	public void onMouseExited(MouseEvent e) {
		if (e.getSource() instanceof ChatPartnerPanel) {
			ChatPartnerPanel panel = (ChatPartnerPanel) e.getSource();
			panel.onMouseExited();
		} else if (e.getSource() instanceof JButton) {
			JButton button = (JButton) e.getSource();
			for (ChatPartnerPanel panel : chatPartnersPanel.getChatPartnerPanels()) {
				if (panel.getChatPartner().equals(button.getActionCommand())) {
					panel.onMouseEntered();
				}
			}
		}
	}

	public void onKeyTyped(KeyEvent e) {
		if (e.getSource() instanceof ResponsiveJTextField) {
			ResponsiveJTextField textField = (ResponsiveJTextField) e.getSource();
			if (Const.MENU_ADD_CHAT.equals(textField.getActionCommand())) {
				if (e.getKeyChar() == KeyEvent.VK_ENTER) {
					chatPartnersPanel.addOrUpdateChatPartner(newChatDialog.getTextFieldText());
					chatPartnersPanel.changeChatPartner(newChatDialog.getTextFieldText());
					messagesChatPanel.changeChatPartner(newChatDialog.getTextFieldText());
					newChatDialog.setVisible(false);
				}
			} else if (Const.SEND_MESSAGE.equals(textField.getActionCommand())) {
				if (e.getKeyChar() == KeyEvent.VK_ENTER) {
					if (textField.getText().length() > 0) {
						easyChat.sendChatMessage(
								"/msg " + messagesChatPanel.getChatPartner() + " " + textField.getText());
						textField.setText("Text eingeben...");
						textField.setCaretPosition(0);
					}
				} else {
					if (textField.getText().contains("Text eingeben...")) {
						textField.setText(textField.getText().replaceAll("Text eingeben...", ""));
					}
				}
			}
		}

	}

	public void onActionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			JButton button = (JButton) e.getSource();
			if (Const.MENU_ADD_CHAT.equals(button.getActionCommand())) {
				chatPartnersPanel.addOrUpdateChatPartner(newChatDialog.getTextFieldText());
				messagesChatPanel.changeChatPartner(newChatDialog.getTextFieldText());
				newChatDialog.setVisible(false);
			} else {
				chatPartnersPanel.removeChatPartner(button.getActionCommand());
				ChatPartnerPanel[] chatPartnerPanels = chatPartnersPanel.getChatPartnerPanels();
				if (chatPartnerPanels.length > 0) {
					chatPartnerPanels[0].setSelected(true);
					messagesChatPanel.changeChatPartner(chatPartnerPanels[0].getChatPartner());
				} else {
					messagesChatPanel.changeChatPartner("");
				}
			}
		}
	}

	public void onStateChanged(ChangeEvent e) {
		if (e.getSource() instanceof JSlider) {
			JSlider slider = (JSlider) e.getSource();
			this.fontSize = slider.getValue();
			setFontSize(this.fontSize);
		}
	}

	public void setFontSize(int size) {
		messagesChatPanel.setFontSizes(size);
		chatPartnersPanel.setFontSize(size);
	}

	public static void changeFontSize(JComponent c, int size) {
		Font newFont = new Font(c.getFont().getFontName(), c.getFont().getStyle(), size);
		c.setFont(newFont);
	}

}
