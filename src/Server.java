import java.util.ArrayList;
import java.util.List;

public class Server {

	private String serverName;
	private String rankPattern;
	private String playerNamePattern;
	private String messageToMePattern;
	private String messageFromMePattern;
	private List<String> serverMessagesPattern = new ArrayList<String>();
	private List<String> otherPatterns = new ArrayList<String>();

	public void addServerMessagePattern(String pattern) {
		serverMessagesPattern.add(pattern);
	}

	public void addOtherPattern(String pattern) {
		otherPatterns.add(pattern);
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public String getRankPattern() {
		return rankPattern;
	}

	public void setRankPattern(String rankPattern) {
		this.rankPattern = rankPattern;
	}

	public String getPlayerNamePattern() {
		return playerNamePattern;
	}

	public void setPlayerNamePattern(String playerNamePattern) {
		this.playerNamePattern = playerNamePattern;
	}

	public String getMessageToMePattern() {
		return messageToMePattern;
	}

	public void setMessageToMePattern(String messageToMePattern) {
		this.messageToMePattern = messageToMePattern;
	}

	public String getMessageFromMePattern() {
		return messageFromMePattern;
	}

	public void setMessageFromMePattern(String messageFromMePattern) {
		this.messageFromMePattern = messageFromMePattern;
	}

	public List<String> getServerMessagesPattern() {
		return serverMessagesPattern;
	}

	public void setServerMessagesPattern(List<String> serverMessagesPattern) {
		this.serverMessagesPattern = serverMessagesPattern;
	}

	public List<String> getOtherPatterns() {
		return otherPatterns;
	}

	public void setOtherPatterns(List<String> otherPatterns) {
		this.otherPatterns = otherPatterns;
	}

}
