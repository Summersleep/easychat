import java.awt.Adjustable;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.util.Arrays;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

public class ChatMessageScrollPanel extends GradientScrollPanel {

	private ChatGui chatGui;
	private JPanel scrollPanelContent = new JPanel();
	private JPanel extraLayoutPanel = new JPanel();

	// private HashMap<String, Integer> storedViewPointPositions = new
	// HashMap<String, Integer>();

	public ChatMessageScrollPanel(ChatGui chatGui) {
		super(Const.BORDER_WIDTH, Const.COLOR_WHITE_LEFT, Const.COLOR_WHITE_MIDDLE, Const.COLOR_WHITE_RIGHT);
		createGui();
		this.chatGui = chatGui;
	}

	public void createGui() {
		this.setOpaque(false);
		this.getVerticalScrollBar().setPreferredSize(new Dimension(0, 0));
		this.getViewport().setBorder(null);
		this.getViewport().setOpaque(false);
		this.setBorder(null);
		this.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		this.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		this.getVerticalScrollBar().setUnitIncrement(16);
		this.getViewport().add(extraLayoutPanel);

		extraLayoutPanel.setLayout(new BorderLayout());
		extraLayoutPanel.setOpaque(false);
		extraLayoutPanel.add(scrollPanelContent, BorderLayout.NORTH);

		scrollPanelContent.setOpaque(false);
		scrollPanelContent.setBorder(new EmptyBorder(10, 10, 10, 10));
		scrollPanelContent.setLayout(new BoxLayout(scrollPanelContent, BoxLayout.Y_AXIS));
	}

	public void addChatMessage(Message message, String currentChatPartner) {
		ChatMessagePanel panel = new ChatMessagePanel(chatGui, message);
		panel.setVisible(currentChatPartner.equals(panel.getChatPartner()));

		scrollPanelContent.add(panel);

		if (currentChatPartner.equals(panel.getChatPartner())) {
			this.revalidate();
			scrollPanelTo(-1);
		}

		this.revalidate();
		this.repaint();
	}

	public void showChatMessagesFrom(String newChatPartner, String currentChatPartner) {
		// storedViewPointPositions.put(currentChatPartner,
		// this.getVerticalScrollBar().getValue());

		for (ChatMessagePanel panel : getChatMessagePanels()) {
			panel.setVisible(newChatPartner.equals(panel.getChatPartner()));
		}

		// if (storedViewPointPositions.containsKey(newChatPartner)) {
		// scrollPanelTo(storedViewPointPositions.get(newChatPartner));
		// }

		scrollPanelTo(-1);

		this.revalidate();
		this.repaint();
	}

	public void setFontSize(int size) {
		for (ChatMessagePanel panel : getChatMessagePanels()) {
			panel.setFontSize(size);
		}
	}

	private ChatMessagePanel[] getChatMessagePanels() {
		return Arrays.copyOf(scrollPanelContent.getComponents(), scrollPanelContent.getComponents().length,
				ChatMessagePanel[].class);

	}

	private void scrollPanelTo(int value) {
		JScrollBar verticalBar = this.getVerticalScrollBar();
		AdjustmentListener downScroller = new AdjustmentListener() {
			@Override
			public void adjustmentValueChanged(AdjustmentEvent e) {
				Adjustable adjustable = e.getAdjustable();
				if (value == -1) {
					adjustable.setValue(adjustable.getMaximum());
				} else {
					adjustable.setValue(value);
				}
				verticalBar.removeAdjustmentListener(this);
			}
		};
		verticalBar.addAdjustmentListener(downScroller);
	}

}
