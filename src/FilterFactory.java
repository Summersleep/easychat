import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FilterFactory {

	public static String findFirstMatchForPatternGroup(String filterPattern, String text) {
		Pattern pattern = Pattern.compile(filterPattern);
		Matcher matcher = pattern.matcher(text);

		while (matcher.find()) {
			for (int i = 0; i <= matcher.groupCount(); i++) {
				if (i > 0 && matcher.group(i) != null) {
					return matcher.group(i);
				}
			}
		}

		return "";
	}

	public static Message createMessageFromText(Server server, String text) {

		Message message = new Message();

		message.setRaw(text);
		message.setPlayerName(FilterFactory.findFirstMatchForPatternGroup(server.getPlayerNamePattern(), text));
		message.setPlayerRank(FilterFactory.findFirstMatchForPatternGroup(server.getRankPattern(), text));
		message.setChatPartner(message.getPlayerName());

		String filterResult = FilterFactory.findFirstMatchForPatternGroup(server.getMessageToMePattern(), text);
		if (!"".equals(filterResult)) {
			message.setType(MessageType.MESSAGE_TO_ME);
		} else {
			filterResult = FilterFactory.findFirstMatchForPatternGroup(server.getMessageFromMePattern(), text);
			if (!"".equals(filterResult)) {
				message.setType(MessageType.MESSAGE_FROM_ME);
			} else {
				for (String pattern : server.getServerMessagesPattern()) {
					filterResult = FilterFactory.findFirstMatchForPatternGroup(pattern, text);
					if (!"".equals(filterResult)) {
						message.setType(MessageType.SERVER_MESSAGE);
						break;
					}
				}
				if ("".equals(filterResult)) {
					for (String pattern : server.getOtherPatterns()) {
						filterResult = FilterFactory.findFirstMatchForPatternGroup(pattern, text);
						if (!"".equals(filterResult)) {
							message.setType(MessageType.OTHER_MESSAGE);
							break;
						}
					}
				}
			}
		}

		message.setMessage(filterResult.trim());

		return message;
	}
}
