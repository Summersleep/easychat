import java.awt.FlowLayout;
import java.text.SimpleDateFormat;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

public class ChatMessagePanel extends JPanel {

	private ChatGui chatGui;
	private Message message;

	private JPanel fillerPanel = new JPanel();
	private JPanel messageHolder = new JPanel();
	private JPanel timeLabelHolder = new JPanel();
	private JPanel textAreaHolder = new JPanel();
	private JTextArea textArea = new JTextArea();
	private JLabel timeLabel = new JLabel();

	public ChatMessagePanel(ChatGui chatGui, Message message) {
		super();
		this.chatGui = chatGui;
		this.message = message;
		createGui();
	}

	private void createGui() {

		this.setOpaque(false);
		this.setBorder(new EmptyBorder(5, 5, 5, 5));

		// Check wether we have to align the textarea left or right
		if (message.getType() == MessageType.MESSAGE_FROM_ME) {
			this.setLayout(new FlowLayout(FlowLayout.RIGHT, 0, 0));
			this.add(fillerPanel);
			this.add(messageHolder);
			timeLabelHolder.setBackground(Const.COLOR_SELECTED_RIGHT);
			textAreaHolder.setBackground(Const.COLOR_SELECTED_RIGHT);
		} else {
			this.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
			this.add(messageHolder);
			this.add(fillerPanel);
			timeLabelHolder.setBackground(Const.COLOR_SELECTED_MIDDLE);
			textAreaHolder.setBackground(Const.COLOR_SELECTED_MIDDLE);
		}

		fillerPanel.setOpaque(false);

		messageHolder.setLayout(new BoxLayout(messageHolder, BoxLayout.Y_AXIS));
		messageHolder.add(textAreaHolder);
		messageHolder.add(timeLabelHolder);

		textAreaHolder.setBorder(new EmptyBorder(0, 10, 0, 5));
		textAreaHolder.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 5));
		textAreaHolder.add(textArea);

		timeLabelHolder.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));
		timeLabelHolder.add(timeLabel);

		textArea.setEditable(false);
		textArea.setOpaque(false);
		textArea.setFont(new JLabel().getFont());
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setBackground(Const.COLOR_SELECTED_MIDDLE);
		textArea.setText(message.getMessage());
		textArea.setColumns(20);
		ChatGui.changeFontSize(textArea, chatGui.fontSize);

		timeLabel.setText(new SimpleDateFormat("HH:mm").format(message.getTimeStampInDateFormat()).toString());
		ChatGui.changeFontSize(timeLabel, chatGui.fontSize);
	}

	public String getChatPartner() {
		return message.getChatPartner();
	}

	public void setFontSize(int size) {
		ChatGui.changeFontSize(textArea, size);
		ChatGui.changeFontSize(timeLabel, size);
	}

}
