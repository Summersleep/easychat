import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

public class GradientBlinkPanel extends GradientPanel {

	private Color firstColorLeft;
	private Color firstColorMid;
	private Color firstColorRight;

	private Color secondColorLeft;
	private Color secondColorMid;
	private Color secondColorRight;

	private boolean blink = false;

	private int blinkCycleMS = 1000;

	public GradientBlinkPanel(int borderWidth, Color firstColorLeft, Color firstColorMid, Color firstColorRight,
			Color secondColorLeft, Color secondColorMid, Color secondColorRight) {
		super(borderWidth, firstColorLeft, firstColorMid, firstColorRight);

		this.firstColorLeft = firstColorLeft;
		this.firstColorMid = firstColorMid;
		this.firstColorRight = firstColorRight;

		this.secondColorLeft = secondColorLeft;
		this.secondColorMid = secondColorMid;
		this.secondColorRight = secondColorRight;

		createGuiUpdateTimer();
	}

	public void setBlinking(boolean blink) {
		this.blink = blink;
	}

	private void createGuiUpdateTimer() {
		GradientBlinkPanel self = this;
		Timer SimpleTimer = new Timer(blinkCycleMS, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (blink) {
					if (self.colorLeft == self.secondColorLeft) {
						self.setGradientColors(self.firstColorLeft, self.firstColorMid, self.firstColorRight);
					} else {
						self.setGradientColors(self.secondColorLeft, self.secondColorMid, self.secondColorRight);
					}
				}
			}
		});
		SimpleTimer.start();
	}
}
