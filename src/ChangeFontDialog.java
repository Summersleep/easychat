import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Base64;

import javax.imageio.ImageIO;
import javax.swing.JDialog;
import javax.swing.JSlider;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class ChangeFontDialog extends JDialog implements ChangeListener {

	private ChatGui chatGui;
	private GradientPanel sliderPanelHolder = new GradientPanel(Const.BORDER_WIDTH, Const.COLOR_WHITE_LEFT,
			Const.COLOR_WHITE_MIDDLE, Const.COLOR_WHITE_RIGHT);

	private JSlider slider;

	public ChangeFontDialog(ChatGui chatGui) {
		this.chatGui = chatGui;
		slider = new JSlider(JSlider.HORIZONTAL, 10, 20, chatGui.fontSize);
		createGui();
		this.pack();
	}

	private void createGui() {
		this.setTitle(Const.MENU_CHANGE_FONT);
		this.setLocationRelativeTo(null);
		this.setAlwaysOnTop(true);

		try {
			BufferedImage img = ImageIO
					.read(new ByteArrayInputStream(Base64.getDecoder().decode(Const.LOGO_BASE_64.getBytes())));
			this.setIconImage(img);
		} catch (IOException e) {
			e.printStackTrace();
		}

		this.add(sliderPanelHolder);

		sliderPanelHolder.setOpaque(false);
		sliderPanelHolder.add(slider);
		sliderPanelHolder.setBorder(new EmptyBorder(5, 10, 5, 10));

		slider.setMajorTickSpacing(5);
		slider.setMinorTickSpacing(1);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.setOpaque(false);
		slider.addChangeListener(this);
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		chatGui.onStateChanged(e);
	}

}
