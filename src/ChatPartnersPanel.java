import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Base64;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class ChatPartnersPanel extends JPanel {

	private ChatGui chatGui;

	public static int CHAT_PARTNER_LIST_WIDTH = 140;
	public static int CHAT_WIDTH = 300;

	private GradientPanel panelTop = new GradientPanel(Const.BORDER_WIDTH, Const.COLOR_GREEN_LEFT,
			Const.COLOR_GREEN_MIDDLE, Const.COLOR_GREEN_RIGHT);
	GradientPanel panelBottom = new GradientPanel(Const.BORDER_WIDTH, Const.COLOR_GREEN_LEFT, Const.COLOR_GREEN_MIDDLE,
			Const.COLOR_GREEN_RIGHT);

	private JPanel panelOptions = new JPanel();
	private ChatPartnersScrollPanel chatPartnersScrollpanel;

	private ResponsiveJLabel labelAddChat;
	private ResponsiveJLabel labelChangeFontSize;

	public ChatPartnersPanel(ChatGui chatGui) {
		super();
		this.chatGui = chatGui;
		this.chatPartnersScrollpanel = new ChatPartnersScrollPanel(chatGui);
		createGui();
	}

	public void createGui() {
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.add(panelTop);
		this.add(chatPartnersScrollpanel);
		this.add(panelBottom);

		try {
			BufferedImage img = ImageIO
					.read(new ByteArrayInputStream(Base64.getDecoder().decode(Const.ADD_CHAT_BASE_64.getBytes())));
			labelAddChat = new ResponsiveJLabel(chatGui, img, Const.MENU_ADD_CHAT, Const.COLOR_BLINK_MIDDLE);
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			BufferedImage img = ImageIO
					.read(new ByteArrayInputStream(Base64.getDecoder().decode(Const.FONT_SIZE_BASE_64.getBytes())));
			labelChangeFontSize = new ResponsiveJLabel(chatGui, img, Const.MENU_CHANGE_FONT, Const.COLOR_BLINK_MIDDLE);
		} catch (IOException e) {
			e.printStackTrace();
		}

		panelTop.setLayout(new BoxLayout(panelTop, BoxLayout.Y_AXIS));
		panelTop.add(panelOptions);
		panelTop.setPreferredSize(new Dimension(CHAT_PARTNER_LIST_WIDTH, 60));
		panelTop.setMaximumSize(new Dimension(9999, 60));
		panelTop.setMinimumSize(new Dimension(CHAT_PARTNER_LIST_WIDTH, 60));

		labelAddChat.setToolTipText(Const.MENU_ADD_CHAT);
		labelChangeFontSize.setToolTipText(Const.MENU_CHANGE_FONT);

		panelOptions.setOpaque(false);
		panelOptions.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
		panelOptions.setBorder(new EmptyBorder(4, 4, 4, 4));
		panelOptions.add(labelChangeFontSize);
		panelOptions.add(labelAddChat);

		panelBottom.setPreferredSize(new Dimension(CHAT_PARTNER_LIST_WIDTH, Const.BORDER_WIDTH));
		panelBottom.setMaximumSize(new Dimension(9999, Const.BORDER_WIDTH));
		panelBottom.setMinimumSize(new Dimension(CHAT_PARTNER_LIST_WIDTH, Const.BORDER_WIDTH));
	}

	public void addOrUpdateChatPartner(String chatPartner) {
		chatPartnersScrollpanel.addOrUpdateChatPartner(chatPartner);
	}

	public void removeChatPartner(String chatPartner) {
		chatPartnersScrollpanel.removeChatPartner(chatPartner);
	}

	public void setFontSize(int size) {
		for (ChatPartnerPanel panel : getChatPartnerPanels()) {
			panel.setFontSize(size);
		}
	}

	public void changeChatPartner(String chatPartner) {
		for (ChatPartnerPanel currPanel : getChatPartnerPanels()) {
			currPanel.setSelected(chatPartner.equals(currPanel.getChatPartner()));
		}
	}

	public ChatPartnerPanel[] getChatPartnerPanels() {
		JPanel chatpartnersHolder = (JPanel) chatPartnersScrollpanel.getViewport().getComponent(0);
		return Arrays.copyOf(chatpartnersHolder.getComponents(), chatpartnersHolder.getComponents().length,
				ChatPartnerPanel[].class);
	}

}
