import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class EasyChat {

	private ChatGui gui;

	public EasyChat() {
		init();
	}

	private void init() {

		gui = new ChatGui(this);

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}

		gui.setVisible(true);

		this.addNewMessage(new Message(
				"Hi, _Skewy hier. Viel Spass mit meinem Hilfstool. Besucht mich mal auf GrieferGames.net, CityBuild 5 :)"));

	}

	public void addNewMessage(String text) {
		if (!"".equals(text.trim())) {
			Message message = FilterFactory.createMessageFromText(getGrieferGamesServer(), text);

			String[] forbiddenNames = { "accept", "bank", "annehmen", "grieferbank", "griefergamesbank" };
			boolean addMessage = true;
			for (String forbiddenName : forbiddenNames) {
				if (message.getChatPartner().toLowerCase().contains(forbiddenName)) {
					addMessage = false;
					break;
				}
			}

			if (addMessage && !"".equals(message.getMessage())) {
				gui.addNewMessage(message);
			}
		}
	}

	public void addNewMessage(Message message) {
		if (!"".equals(message.getMessage())) {
			gui.addNewMessage(message);
		}
	}

	// DUMMY
	public void sendChatMessage(String text) {
		// this.mc.thePlayer.sendChatMessage("/msg _GRENZGAENGER02 test");
		// will be added through bytecodemanipulation, because we do not know
		// variablenames from minecraft.jar just yet. but later at runtime
	}

	public static Server getGrieferGamesServer() {
		Server server = new Server();
		server.setServerName("GrieferGames.net");
		server.setPlayerNamePattern("(?:\\[(?:.*) \\| (.*) -> mir\\])|(?:\\[mir -> (?:.*) \\| (.*)\\])");
		server.setRankPattern("(?:\\[(.*) \\| (?:.*) mir\\])|(?:\\[mir -> (.*) \\| (?:.*)\\])");
		server.setMessageToMePattern("(?:^\\[(?:.*) \\| (?:.*) mir\\](.*))");
		server.setMessageFromMePattern("(?:^\\[mir -> (?:.*) \\| (?:.*)\\](.*))");
		server.addServerMessagePattern("(^\\[ClearLag\\].*)");
		server.addServerMessagePattern("(^\\[MobRemover\\].*)");
		server.addServerMessagePattern("(^Kontostand.*)");
		server.addServerMessagePattern("(^\\[GrieferBank\\].*)");
		server.addOtherPattern("(.*such.*)");
		server.addOtherPattern("(.*verkauf.*)");
		server.addOtherPattern("(.*kauf.*)");
		server.addOtherPattern("(.*brauch.*)");
		return server;
	}

	public static void main(String[] args) {
		new EasyChat();
	}
}
