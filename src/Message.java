import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class Message {

	private String raw = "";
	private String playerRank = "";
	private String chatPartner = "";
	private String playerName = "";
	private String message = "";
	private LocalDateTime timeStamp = LocalDateTime.now();
	private MessageType type = MessageType.OTHER_MESSAGE;

	public Message() {

	}

	public Date getTimeStampInDateFormat() {
		return Date.from(timeStamp.atZone(ZoneId.systemDefault()).toInstant());
	}

	public Message(String message, MessageType type) {
		this.message = message;
		this.type = type;
		if (this.type == MessageType.OTHER_MESSAGE) {
			this.chatPartner = Const.CHAT_PARTNER_SONSTIGE;
		} else if (this.type == MessageType.SERVER_MESSAGE) {
			this.chatPartner = Const.CHAT_PARTNER_SERVER;
		}
	}

	public Message(String message) {
		this.message = message;
		this.chatPartner = Const.CHAT_PARTNER_SONSTIGE;
	}

	public Message(String chatName, String message) {
		this.chatPartner = chatName;
		this.message = message;
	}

	public String getRaw() {
		return raw;
	}

	public void setRaw(String raw) {
		this.raw = raw;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getChatPartner() {
		return chatPartner;
	}

	public void setChatPartner(String chatPartner) {
		this.chatPartner = chatPartner;
	}

	public String getPlayerRank() {
		return playerRank;
	}

	public void setPlayerRank(String playerRank) {
		this.playerRank = playerRank;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public MessageType getType() {
		return type;
	}

	public void setType(MessageType type) {
		this.type = type;
		if (this.type == MessageType.OTHER_MESSAGE) {
			this.chatPartner = Const.CHAT_PARTNER_SONSTIGE;
		} else if (this.type == MessageType.SERVER_MESSAGE) {
			this.chatPartner = Const.CHAT_PARTNER_SERVER;
		}
	}

	public LocalDateTime getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(LocalDateTime timeStamp) {
		this.timeStamp = timeStamp;
	}

}