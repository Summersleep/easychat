import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JScrollPane;

public class GradientScrollPanel extends JScrollPane {

	private int borderWidth;
	private Color colorLeft;
	private Color colorMid;
	private Color colorRight;

	public GradientScrollPanel() {
		super();
	}

	public GradientScrollPanel(int borderWidth, Color colorLeft, Color colorMid, Color colorRight) {
		super();
		this.borderWidth = borderWidth;
		setGradientColors(colorLeft, colorMid, colorRight);
	}

	@Override
	public Dimension getPreferredSize() {
		Dimension preferred = super.getPreferredSize();
		Dimension minimum = getMinimumSize();
		Dimension maximum = getMaximumSize();
		preferred.width = Math.min(Math.max(preferred.width, minimum.width), maximum.width);
		preferred.height = Math.min(Math.max(preferred.height, minimum.height), maximum.height);
		return preferred;
	}

	public void setBorderWidth(int borderWidth) {
		this.borderWidth = borderWidth;
	}

	public void setGradientColors(Color lightColor, Color mainColor, Color darkColor) {
		this.colorLeft = lightColor;
		this.colorMid = mainColor;
		this.colorRight = darkColor;
		this.repaint();
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		if (colorLeft != null && colorMid != null && colorRight != null) {
			Graphics2D g2d = (Graphics2D) g.create();
			int w = getWidth();
			int h = getHeight();

			// left Border
			GradientPaint gp = new GradientPaint(0, 0, colorLeft, borderWidth, 0, colorMid, true);
			g2d.setPaint(gp);
			g2d.fillRect(0, 0, borderWidth, h);

			// middle Paint
			g2d.setPaint(colorMid);
			g2d.fillRect(borderWidth, 0, w - (borderWidth * 2), h);

			// Right Border
			gp = new GradientPaint(0, 0, colorMid, borderWidth, 0, colorRight, true);
			g2d.setPaint(gp);
			g2d.fillRect(w - borderWidth, 0, borderWidth, h);

			g2d.dispose();
		}
	}

}
