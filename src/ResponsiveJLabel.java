import java.awt.Color;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class ResponsiveJLabel extends JLabel implements MouseListener {

	private ChatGui chatGui;
	private String menuType;

	public ResponsiveJLabel(ChatGui chatGui, Image image, String text, Color hoverColor) {
		super(new ImageIcon(image));
		this.chatGui = chatGui;
		menuType = text;
		this.setBackground(hoverColor);
		this.addMouseListener(this);

	}

	public String getMenuType() {
		return menuType;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		chatGui.onMouseClicked(e);

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		this.setOpaque(true);
		this.revalidate();
		this.repaint();

	}

	@Override
	public void mouseExited(MouseEvent e) {
		this.setOpaque(false);
		this.revalidate();
		this.repaint();
	}

}
