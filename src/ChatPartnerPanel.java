import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class ChatPartnerPanel extends GradientBlinkPanel implements MouseListener, MouseMotionListener, ActionListener {

	private ChatGui chatGui;

	private JPanel panel = new JPanel();

	private JLabel labelChatPartner = new JLabel();

	private JButton buttonClose = new JButton("x");

	private boolean selected = false;
	private String chatPartner;
	private int undreadMessagesCount = 0;

	public ChatPartnerPanel(ChatGui chatGui, String chatPartner) {
		super(Const.BORDER_WIDTH, Const.COLOR_WHITE_LEFT, Const.COLOR_WHITE_MIDDLE, Const.COLOR_WHITE_RIGHT,
				Const.COLOR_BLINK_LEFT, Const.COLOR_BLINK_MIDDLE, Const.COLOR_BLINK_RIGHT);

		this.chatGui = chatGui;
		this.chatPartner = chatPartner;

		createGui();

		onNewMessage();
	}

	private void createGui() {
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.add(panel);
		// this.add(panelMiddle);
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		// this.add(panelBottom);

		panel.setOpaque(false);
		panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
		panel.setBorder(new EmptyBorder(4, 10, 4, 15));
		panel.add(labelChatPartner);
		panel.add(Box.createHorizontalGlue());
		panel.add(buttonClose);

		buttonClose.setOpaque(true);
		buttonClose.setBackground(Const.COLOR_WHITE_MIDDLE);
		buttonClose.addActionListener(this);
		buttonClose.setActionCommand(chatPartner);
		buttonClose.setContentAreaFilled(false);
		buttonClose.addMouseListener(this);
		buttonClose.addMouseMotionListener(this);
		buttonClose.setBorder(new EmptyBorder(1, 5, 1, 5));
		buttonClose.setFocusable(false);
		ChatGui.changeFontSize(buttonClose, chatGui.fontSize);

		labelChatPartner.setText(chatPartner);
		ChatGui.changeFontSize(labelChatPartner, chatGui.fontSize);
	}

	private void increaseMessageCount() {
		undreadMessagesCount++;
		if (!selected) {
			this.setBlinking(true);
		}
	}

	private void resetMessageCount() {
		this.setBlinking(false);
		undreadMessagesCount = 0;
	}

	private boolean areThereUnreadMessages() {
		return undreadMessagesCount > 0;
	}

	public String getChatPartner() {
		return chatPartner;
	}

	public void onNewMessage() {
		if (!selected)
			increaseMessageCount();
	}

	public void setSelected(boolean select) {
		this.selected = select;
		if (selected) {
			this.setGradientColors(Const.COLOR_SELECTED_LEFT, Const.COLOR_SELECTED_MIDDLE, Const.COLOR_SELECTED_RIGHT);
			resetMessageCount();
		} else {
			this.setGradientColors(Const.COLOR_WHITE_LEFT, Const.COLOR_WHITE_MIDDLE, Const.COLOR_WHITE_RIGHT);
		}
	}

	public void onMouseEntered() {
		if (selected) {
			this.setGradientColors(Const.COLOR_SELECTED_LEFT, Const.COLOR_SELECTED_MIDDLE, Const.COLOR_SELECTED_RIGHT);
		} else {
			this.setGradientColors(Const.COLOR_HOVER_LEFT, Const.COLOR_HOVER_MIDDLE, Const.COLOR_HOVER_RIGHT);
		}
		this.setBlinking(false);
	}

	public void onMouseExited() {
		if (selected) {
			this.setGradientColors(Const.COLOR_SELECTED_LEFT, Const.COLOR_SELECTED_MIDDLE, Const.COLOR_SELECTED_RIGHT);
		} else {
			this.setGradientColors(Const.COLOR_WHITE_LEFT, Const.COLOR_WHITE_MIDDLE, Const.COLOR_WHITE_RIGHT);

		}
		this.setBlinking(areThereUnreadMessages());
	}

	public void onCloseButtonEntered() {
		this.setGradientColors(Const.COLOR_RED_LEFT, Const.COLOR_RED_MIDDLE, Const.COLOR_RED_RIGHT);
		this.setBlinking(false);
	}

	public void setFontSize(int size) {
		ChatGui.changeFontSize(buttonClose, size);
		ChatGui.changeFontSize(labelChatPartner, size);
	}

	@Override
	public void mouseDragged(MouseEvent e) {

	}

	@Override
	public void mouseMoved(MouseEvent e) {

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		chatGui.onMouseClicked(e);
	}

	@Override
	public void mousePressed(MouseEvent e) {

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		chatGui.onMouseEntered(e);
	}

	@Override
	public void mouseExited(MouseEvent e) {
		chatGui.onMouseExited(e);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		chatGui.onActionPerformed(e);
	}

}
