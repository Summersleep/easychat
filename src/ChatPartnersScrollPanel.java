import java.awt.Component;
import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class ChatPartnersScrollPanel extends GradientScrollPanel {

	private ChatGui chatGui;

	private JPanel scrollPanelContent = new JPanel();

	public ChatPartnersScrollPanel(ChatGui chatGui) {
		super(Const.BORDER_WIDTH, Const.COLOR_WHITE_LEFT, Const.COLOR_WHITE_MIDDLE, Const.COLOR_WHITE_RIGHT);
		this.chatGui = chatGui;
		createGui();
	}

	public void createGui() {
		this.setOpaque(false);
		this.getVerticalScrollBar().setPreferredSize(new Dimension(0, 0));
		this.getViewport().setBorder(null);
		this.getViewport().setOpaque(false);
		this.setBorder(null);
		this.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		this.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		this.getVerticalScrollBar().setUnitIncrement(16);
		this.getViewport().add(scrollPanelContent);

		scrollPanelContent.setOpaque(false);
		scrollPanelContent.setLayout(new BoxLayout(scrollPanelContent, BoxLayout.Y_AXIS));
	}

	public void addOrUpdateChatPartner(String chatPartner) {
		ChatPartnerPanel chatpartnerPanel = getChatPartnerPanel(chatPartner);
		if (chatpartnerPanel != null) {
			chatpartnerPanel.onNewMessage();
		} else {
			scrollPanelContent.add(new ChatPartnerPanel(chatGui, chatPartner));
			this.revalidate();
			this.repaint();
		}
	}

	public void removeChatPartner(String chatPartner) {
		ChatPartnerPanel chatpartnerPanel = getChatPartnerPanel(chatPartner);
		if (chatpartnerPanel != null) {
			scrollPanelContent.remove(chatpartnerPanel);
			this.revalidate();
			this.repaint();
		}
	}

	private ChatPartnerPanel getChatPartnerPanel(String chatPartner) {
		for (Component c : scrollPanelContent.getComponents()) {
			if (c instanceof ChatPartnerPanel) {
				ChatPartnerPanel chatPartnerPanel = (ChatPartnerPanel) c;
				if (chatPartnerPanel.getChatPartner().equals(chatPartner)) {
					return chatPartnerPanel;
				}
			}
		}
		return null;
	}

}
