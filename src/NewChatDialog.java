import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Base64;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JDialog;

public class NewChatDialog extends JDialog implements ActionListener, KeyListener {

	private ChatGui chatGui;

	private GradientPanel panel = new GradientPanel(Const.BORDER_WIDTH, Const.COLOR_WHITE_LEFT,
			Const.COLOR_WHITE_MIDDLE, Const.COLOR_WHITE_RIGHT);

	private ResponsiveJTextField newChatPartnerTextField = new ResponsiveJTextField(Const.MENU_ADD_CHAT);
	private JButton addButton = new JButton("OK");

	public NewChatDialog(ChatGui chatGui) {
		this.chatGui = chatGui;
		createGui();
	}

	private void createGui() {
		this.setTitle(Const.MENU_ADD_CHAT);
		this.setLocationRelativeTo(null);
		this.setAlwaysOnTop(true);

		try {
			BufferedImage img = ImageIO
					.read(new ByteArrayInputStream(Base64.getDecoder().decode(Const.LOGO_BASE_64.getBytes())));
			this.setIconImage(img);
		} catch (IOException e) {
			e.printStackTrace();
		}

		newChatPartnerTextField.setPreferredSize(new Dimension(100, 25));
		newChatPartnerTextField.setMinimumSize(new Dimension(100, 25));
		newChatPartnerTextField.setMaximumSize(new Dimension(100, 25));
		newChatPartnerTextField.addKeyListener(this);
		newChatPartnerTextField.setActionCommand(Const.MENU_ADD_CHAT);

		addButton.addActionListener(this);
		addButton.setActionCommand(Const.MENU_ADD_CHAT);
		addButton.setPreferredSize(new Dimension(75, 25));
		addButton.setMinimumSize(new Dimension(75, 25));
		addButton.setMaximumSize(new Dimension(75, 25));

		panel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		panel.add(newChatPartnerTextField);
		panel.add(addButton);

		this.add(panel);
		this.pack();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		this.chatGui.onActionPerformed(e);
	}

	public String getTextFieldText() {
		return newChatPartnerTextField.getText();
	}

	public void focusTextField() {
		newChatPartnerTextField.requestFocus();
	}

	public void resetTextField() {
		newChatPartnerTextField.setText("");
	}

	@Override
	public void keyTyped(KeyEvent e) {
		this.chatGui.onKeyTyped(e);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

}
